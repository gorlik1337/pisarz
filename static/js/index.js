
function createLogBox(message, id) {   
    var logbox = document.createElement("div");
    logbox.className = "logbox";
    
    var button = document.createElement("button");
    button.className = "btn btn-danger btn-sm";
    button.type = "button";
	button.innerText = 'del'
	button.setAttribute('onclick', 'deleteLog('+id+');');
    
    var control = document.createElement("div");
    control.className = "control";
    control.appendChild(button);
    
    var msg = document.createElement("div");
    msg.className = 'text';
    msg.innerText = message;
    
    logbox.appendChild(control);
    logbox.appendChild(msg);

	return logbox;
}
function updateLogs() {
    $.ajax({
		url: '/showLogs',
		type: 'GET',
		success: function(response) {
			obj = JSON.parse(response);
			console.log(obj.data);
			logs = obj.data

			for (i = consoleFrame.childElementCount; i > 0 ; i--) {
				consoleFrame.removeChild(consoleFrame.firstElementChild);
			}

			for (i = 0; i < obj.data.length; i++) {
				consoleFrame.append(createLogBox(logs[i],i));
			}

			if (consoleFrame.children.length <= 20){
				consoleFrame.scrollTo(0,consoleFrame.scrollHeight); 
			}else {
				consoleFrame.scrollTo(0,600); 
			}

		},
		error: function(error) {
			console.log(error);
		}
	});
}


function deleteLog(xid) {
    $.ajax({
		url: '/delLogs',
		data: {id:xid}, 
		type: 'GET',
		success: function(response) {
			console.log(response);
			updateLogs();
		},
		error: function(error) {
			console.log(error);
		}
	});
}

	

updateLogs();

/*
const updateUptime = function(num) {
	const leadingZero = function(element) {
		if (element < 10) return element = "0" + element;
		return element;
	}
	
	var dt1 = new Date();           
	var dt2 = new Date(parseInt(num+'000'));   
	var ilems1 = dt1.getTime();
	var ilems2 = dt2.getTime();
	var timeDifference = ilems1 - ilems2;

	//console.log(timeDifference+' = '+ilems1+' - '+ilems2);
	
	const msInADay = 24 * 60 * 60 * 1000;
	const eDaysToDate = timeDifference / msInADay;
    const daysToDate = Math.floor(eDaysToDate);

    //musimy tutaj sprawdzic, czy powyzsza zmienna nie jest 0,
    //bo inaczej ponizej bysmy mieli % 0 czyli dzielenie przez 0
    if (daysToDate < 1) {
        daysToDateFix = 1;
    } else {
        daysToDateFix = daysToDate;
    }

    const eHoursToDate = (eDaysToDate % daysToDateFix)*24;
    const hoursToDate = Math.floor(eHoursToDate);

    const eMinutesToDate = (eHoursToDate - hoursToDate)*60;
    const minutesToDate = Math.floor(eMinutesToDate);

    const eSecondsToDate = Math.floor((eMinutesToDate - minutesToDate)*60);
    const secondsToDate = Math.floor(eSecondsToDate);

	const tekst = '  '+hoursToDate+':'+leadingZero(minutesToDate)+'.'+leadingZero(secondsToDate)+'  '+daysToDate+' dni';
	return tekst;		

}
*/

/* ======================================== */
/* LOOPS */
/* ======================================== */
/*
//update uptime number
loopUptimeUpdate=setInterval(function(){
	document.getElementById('uptimevalue').innerHTML = updateUptime(boot_time);
},1000);

//Load more Logs on top consoleFrame
loopLoadMoreLogs=setInterval(function(){
	if (document.getElementById('consoleFrame')) {
		if (!isLoading && consoleFrame.scrollTop == 0 && isSiteFullLoaded) {
			loadMoreLogs();
		}
	}
},100); //0.1 sec
	
	
//load new logs and update charts
loopLoadNewLogs=setInterval(function(){
	if (document.getElementById('consoleFrame')) {
		$.ajax({
			url: '/showNewLogs',
			data: {new:logsTableEndPoint},
			type: 'GET',
			success: function(response) {
				//console.log(response);
				var obj = JSON.parse(response);
				$('.chart.temp').data('easyPieChart').update(obj.charts.temp);
				$('.chart.cpu').data('easyPieChart').update(obj.charts.cpu_usage);
				$('.chart.ram').data('easyPieChart').update(obj.charts.ram_usage);
				
				
				_before = logsTableEndPoint;
				
				for (var id in obj.newLogs) {
					if(obj.newLogs.hasOwnProperty(id)){
						consoleFrame.append(createLogBox(obj.newLogs[id]));
						logsTableEndPoint++;
					}
				}
				if (_before != logsTableEndPoint){
					if (document.getElementById("autoScrollCheckbox").checked){
						//consoleFrame.scrollTo(0,consoleFrame.scrollHeight);
						$("#consoleFrame").animate({ scrollTop: consoleFrame.scrollHeight }, "slow");
					}
				}
				printLostConnection(false);
			},
			error: function(error) {
				printLostConnection(true);
				console.log(error);
			}
		});
	}
},1000); //1 sec	

// I don't know but if delete this logs dont print  ¯\_(ツ)_/¯
window.onload = function () { 
	isSiteFullLoaded = true;
	consoleFrame.scrollTo(0,consoleFrame.scrollHeight); 
}

*/