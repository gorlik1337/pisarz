#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, time
import speech_recognition as sr
from os import path
import datetime, json
from flask import Flask, render_template, request, json, session, flash, redirect, url_for, flash, send_from_directory
from multiprocessing import Process, Value

try:
    import RPi.GPIO as GPIO
except:
    print "============================================================"
    print "Nie udało się zaimportować RPi.GPIO!"
    print "============================================================"
    sys.exit()

import os, signal, subprocess
cmd = 'arecord -D plughw:1,0 -f cd -t wav --use-strftime temp.wav'
FNULL = open(os.devnull, 'wb')


def LEDgreen(state):
    GPIO.output(GPIO_green_LED, toBool(state))
def LEDred(state):
    GPIO.output(GPIO_red_LED, toBool(state))
def LEDyellow(state):
    GPIO.output(GPIO_yellow_LED, toBool(state))
def toBool(state):
    if (state == True or state==1):
        return GPIO.HIGH
    elif (state == False or state==0): 
        return GPIO.LOW 
    else:
        print 'ERROR bool LED'
        raise
def transcribe(filename):
    AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)),filename)

    r = sr.Recognizer()
    with sr.AudioFile(AUDIO_FILE) as source:
        audio = r.record(source)
    #Wyślij próbkę do Google

    try:
        final_result = r.recognize_google(audio,language="pl-PL")
        #print("Rozpoznałam tekst jako: " + str(final_result))
    except sr.UnknownValueError:
        final_result = ""
    except sr.RequestError as e:
        log("Wystąpił błąd; {0}".format(e),'fail')
        final_result = "_error"
    return final_result.encode('utf-8')

GPIO_green_LED = 13
GPIO_red_LED = 26
GPIO_yellow_LED = 19

GPIO_record_button = 6
GPIO_cmd_button = 5

GPIO.setmode(GPIO.BCM)
GPIO.setup(26, GPIO.OUT)
GPIO.setup(19, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)

GPIO.setup(GPIO_record_button, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(GPIO_cmd_button, GPIO.IN, pull_up_down=GPIO.PUD_UP)

class Loging:
    def __init__(self):
       if not os.path.isfile('blocks.json'):
            plik = open('blocks.json', 'w')
            plik.write('{"data": []}')
            plik.close()

        #if not os.path.isfile('out.txt'): 
        #    plik = open('out.txt', 'w') 
        #    plik.write('# '+str(datetime.datetime.now())+'\n')
        #    plik.close() 
    
    def add(self, var):
        with open('blocks.json') as json_data_file:
            data = json.load(json_data_file)

            data['data'].append(var)
            self.saveblocks(data)

    def lastremove(self):
        with open('blocks.json') as json_data_file:
            data = json.load(json_data_file)
            if len(data['data']) > 0:
                data['data'].pop()
            self.saveblocks(data)
        
    def remove(self, id):
        with open('blocks.json') as json_data_file:
            data = json.load(json_data_file)
            if len(data['data']) > id:
                del data['data'][id]
                self.saveblocks(data)
    
    def updateout(self):
        with open('blocks.json') as json_data_file:
            data = json.load(json_data_file)
            
            if len(data['data']) > 0:
                plik = open('out.txt', 'w')
                for p in data['data']: plik.write(p.encode('utf-8')+'\n')
                plik.close()

    def saveblocks(self, data):
        plik = open('blocks.json', 'w')
        plik.write(json.dumps(data))
        plik.close()
        self.updateout()


app = Flask(__name__)
app.secret_key = os.urandom(12)
app.config['SESSION_TYPE'] = 'filesystem'

@app.route("/")
def index():
    return render_template('index.html')


@app.route('/showLogs',methods=['GET'])
def showLogs():
    file = open('blocks.json', 'r') 
    out = ''
    for line in file: 
        out = out + '' + line
        return out
    return 'ERROR showLogs'

@app.route('/delLogs',methods=['GET'])
def delLogs():
    _id = int(request.args['id'])
    if _id < 0:
        _id = 0
    loging = Loging()
    loging.remove(_id)
    return 'ok'

@app.route('/text')
def printouttxt():
    file = open('out.txt', 'r') 
    out = ''
    for line in file: 
        out = out + '' + line
    return '<textarea style="margin: 0px; height: 100%; width: 100%;">'+out+'</textarea>'

#if True:
def buttonmain():
    try:
        LEDgreen(False)
        LEDyellow(False)
        LEDred(False)
        loging = Loging()

        while True:
            if (GPIO.input(GPIO_record_button)==False):
                LEDgreen(False)
                LEDyellow(True)
                print 'Nagrywam Zdanie'
                pro = subprocess.Popen(cmd, stdout=FNULL, shell=True, preexec_fn=os.setsid) 
                time.sleep(3)
                while (GPIO.input(GPIO_record_button)==False):
                    time.sleep(0.1)

                os.killpg(os.getpgid(pro.pid), signal.SIGTERM)
                LEDyellow(False)
                
                text = transcribe('temp.wav')
                loging.add(text)
                print '[LOG] + '+text


                time.sleep(0.1)

            if (GPIO.input(GPIO_cmd_button)==False):
                LEDgreen(False)
                LEDyellow(True)
                print 'Nagrywam POLECENIE!'
                LEDyellow(False)
                time.sleep(0.1)
            
            LEDgreen(True)
            time.sleep(0.1)

    except:
        GPIO.cleanup()
        os.killpg(os.getpgid(pro.pid), signal.SIGTERM)

def launch():
    app.run(debug=False, host= '0.0.0.0')

if __name__ == "__main__":
    p = Process(target=launch)
    p.start()
    buttonmain()
    p.join()

