
import sys, time

import os
import signal
import subprocess

cmd = 'arecord -D plughw:1,0 -f cd -t wav --use-strftime temp.wav'
FNULL = open(os.devnull, 'w')

pro = subprocess.Popen(cmd, stdout=FNULL, 
                       shell=True, preexec_fn=os.setsid) 

a = 0
while(a < 5):
    a += 1
    print a
    time.sleep(1)

os.killpg(os.getpgid(pro.pid), signal.SIGTERM)
